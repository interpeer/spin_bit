/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef SPIN_BIT_GENERATOR_H
#define SPIN_BIT_GENERATOR_H

#ifndef __cplusplus
#error This is a C++, header-only library.
#endif

// #include <cstdint>
// #include <chrono>
// #include <memory>
#include <tuple>

#include <spin_bit/state.h>
#include <spin_bit/vec.h>
#include <spin_bit/packet.h>

namespace spin_bit {

/**
 * A structure for keeping packet metadata, meaning the spin bit itself,
 * the VEC and a full packet number.
 */
template <typename stateT>
struct packet_metadata
{
  using spin_bit_t = typename stateT::spin_bit_t;
  using vec_t = typename stateT::vec_t;
  using packet_number_t = typename stateT::packet_number_t;

  spin_bit_t      spin_bit = {};
  vec_t           vec = {};
  packet_number_t packet_number = {};

  inline uint8_t vec_value() const
  {
    return vec_ops<vec_t>::value(vec);
  }
};

/**
 * The generator base allows for run-time selection of generators.
 */
template <typename stateT>
struct generator_base
{
  using packet_metadata_t = packet_metadata<stateT>;

  using spin_bit_t = typename packet_metadata_t::spin_bit_t;
  using vec_t = typename packet_metadata_t::vec_t;
  using packet_number_t = typename packet_metadata_t::packet_number_t;

  virtual ~generator_base() = default;

  virtual bool consume(packet_metadata_t const & meta) = 0;
  virtual bool consume(spin_bit_t const & spin_bit, vec_t const & vec,
      packet_number_t const & packet_number) = 0;

  virtual std::tuple<bool, packet_metadata_t> produce() = 0;

};

/**
 * The generator keeps state, and produces full packet metadata from that
 * state and a packet number generating function. It can be used in protocol
 * implementations for the client and server side respectively to generate
 * the bits necessary to put into outgoing packets.
 */
template <typename stateT, bool CLIENT>
struct generator : public generator_base<stateT>
{
public:
  using packet_metadata_t = typename generator_base<stateT>::packet_metadata_t;

  using spin_bit_t = typename packet_metadata_t::spin_bit_t;
  using vec_t = typename packet_metadata_t::vec_t;
  using packet_number_t = typename packet_metadata_t::packet_number_t;

  using duration = typename stateT::duration;

  using packet_number_func = std::function<packet_number_t (packet_number_t const &)>;

  static packet_number_t monotonic_inc(packet_number_t const & num)
  {
    return num + 1;
  }

  /**
   * Instanciate with an initial packet number (defaulting to zero), and a
   * function for generating the next packet number. The default here is a
   * function that monotonically increases the packet number. Note that the
   * VEC implementation permits for adding a random amount >1 to the previous
   * packet number, hence the approach via the function.
   */
  inline explicit generator(duration const & delay_timeout,
      packet_number_t initial = packet_number_t{} + 1,
      packet_number_func pn_func = monotonic_inc)
    : m_delay_timeout{delay_timeout}
    , m_last{initial}
    , m_packet_number_func{pn_func}
  {
  }

  virtual ~generator() = default;


  /**
   * Consume an incoming packet metadata, and update internal state from it.
   */
  virtual bool consume(packet_metadata_t const & meta) override final
  {
    return on_incoming_packet(m_state, meta.spin_bit, meta.vec,
        meta.packet_number);
  }

  virtual bool consume(spin_bit_t const & spin_bit, vec_t const & vec,
      packet_number_t const & packet_number) override final
  {
    return on_incoming_packet(m_state, spin_bit, vec, packet_number);
  }



  /**
   * Produce packet metadata for an outgoing packet. The function assumes that
   * this metadata is also transmitted, so updates internal state as if that
   * happened.
   */
  virtual std::tuple<bool, packet_metadata_t>
  produce() override final
  {
    packet_metadata_t meta;

    if (!CLIENT && (m_state.time == typename stateT::time_point{})) {
      return {false, meta};
    }

    // If we never sent the initial packet number, we send it now. Otherwise,
    // we create a new one.
    if (!m_sent_initial) {
      meta.packet_number = m_last;
      m_sent_initial = true;
      if (CLIENT) {
        m_state.new_edge = true;
      }
    }
    else {
      meta.packet_number = m_last = m_packet_number_func(m_last);
    }

    // For the rest of the packet metadata, we use the spin bit implementation.
    on_outgoing_packet<CLIENT, stateT>(m_state, meta.spin_bit, meta.vec, m_delay_timeout);
    return {true, meta};
  }




private:

  duration            m_delay_timeout;
  packet_number_t     m_last;
  packet_number_func  m_packet_number_func;

  bool                m_sent_initial = false;
  stateT              m_state = {};
};


} // namespace spin_bit

#endif // guard
