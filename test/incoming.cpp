/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <spin_bit/packet.h>

template <typename T>
class Incoming : public ::testing::Test {};
TYPED_TEST_SUITE_P(Incoming);


TYPED_TEST_P(Incoming, same_spin_lower_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, false, vec, 3);
  ASSERT_FALSE(ret);
  ASSERT_FALSE(s.spin_bit);
  ASSERT_EQ(42, s.packet_number);
  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Incoming, different_spin_lower_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, true, vec, 3);
  ASSERT_FALSE(ret);
  ASSERT_FALSE(s.spin_bit);
  ASSERT_EQ(42, s.packet_number);
  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Incoming, same_spin_same_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, false, vec, 42);
  ASSERT_FALSE(ret);
  ASSERT_FALSE(s.spin_bit);
  ASSERT_EQ(42, s.packet_number);
  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Incoming, different_spin_same_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, true, vec, 42);
  ASSERT_FALSE(ret);
  ASSERT_FALSE(s.spin_bit);
  ASSERT_EQ(42, s.packet_number);
  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Incoming, same_spin_higher_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, false, vec, 43);
  ASSERT_FALSE(ret);
  ASSERT_FALSE(s.spin_bit);
  ASSERT_EQ(42, s.packet_number);
  ASSERT_FALSE(s.new_edge);
}


TYPED_TEST_P(Incoming, different_spin_higher_packet_number)
{
  using state = spin_bit::state<
    bool,
    TypeParam,
    uint32_t
  >;

  TypeParam vec;

  state s;
  s.spin_bit = false;
  s.packet_number = 42;

  auto ret = spin_bit::on_incoming_packet(s, true, vec, 43);
  ASSERT_TRUE(ret);
  ASSERT_TRUE(s.spin_bit);
  ASSERT_EQ(43, s.packet_number);
  ASSERT_TRUE(s.new_edge);
}


REGISTER_TYPED_TEST_SUITE_P(Incoming,
    same_spin_lower_packet_number,
    different_spin_lower_packet_number,
    same_spin_same_packet_number,
    different_spin_same_packet_number,
    same_spin_higher_packet_number,
    different_spin_higher_packet_number
);

typedef ::testing::Types<
  bool [2],
  std::bitset<2>
> test_types;
INSTANTIATE_TYPED_TEST_SUITE_P(spin_bit, Incoming, test_types);
