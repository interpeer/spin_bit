/**
 * This file is part of spin_bit.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt).
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <gtest/gtest.h>

#include <bitset>

#include <spin_bit/state.h>

TEST(State, with_array)
{
  using state = spin_bit::state<
    bool,
    bool [2],
    uint32_t
  >;

  state s;
  ASSERT_FALSE(s.vec[0]);
  ASSERT_FALSE(s.vec[1]);
}


TEST(State, with_bitset)
{
  using state = spin_bit::state<
    bool,
    std::bitset<2>,
    uint32_t
  >;

  state s;
  ASSERT_FALSE(s.vec[0]);
  ASSERT_FALSE(s.vec[1]);
}
